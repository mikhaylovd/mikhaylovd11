import java.util.*;
import java.util.stream.Stream;
class Frequency{
    String s;
    Long freq;
    Frequency(String s, Long freq){this.s = s; this.freq = freq;}
    public String toString (){
        return s;
    }
}
public class Main {
    static String deleteGrammar(String str){
        char[] grammar = {'!', ',','.','?','"',':',';'};
        for (char c : grammar){
            str = str.replace(Character.toString(c), "");
        }
        return str;
    }
    public static Frequency frequencyCount (List<String> list, String word){
        return new Frequency(word, list.stream()
                .filter(x->x.equals(word))
                .count());
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<String> text = Stream.of((scanner.nextLine())
                .split(" "))
                .toList();
        text = text.stream()
                .map(x -> deleteGrammar(x).toLowerCase())
                .toList();
        List<String> finalText = text;
        Stream<Frequency> frequencyStream= text.stream()
                .distinct()
                .map(x->frequencyCount(finalText,x));
        frequencyStream
                .sorted((y1,y2)->y1.s.compareTo(y2.s))
                .sorted((x1,x2) -> (int) (x2.freq - x1.freq))
                .limit(10)
                .forEach(System.out::println);
    }
}
